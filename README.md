# Challenge Chapter 2
Hãy áp dụng kiến thức về Struct, Slice,... trong GOLANG, hoặc các cấu trúc dữ liệu khác do bạn tự đề xuất
để đọc file 'TOEIC_Exam1_Done.xlsx' (có thể giải nén, và đọc từ thư mục). Sau đó hiển thị lại thông tin đã
đọc được ra màn hình để xem ít nhất các thông tin sau:

- Số Phần (Part) của đề thi.
- Số câu hỏi của từ Part.
- Danh sách các câu hỏi và các mục trải lời (Answer options).

**Repo gồm các file:**
1. TOEIC_Exam1_Done.xlsx là file excel chứa bộ đề thi TOEIC cần lọc câu hỏi và câu trả lời.
2. **toeic_answer.py** là đoạn code đọc file excel bộ đề thi và xuất ra file **toeic_exam1_answer.xlsx**.
3. toeic_exam1_answer.xlsx là file excel chứa đáp án của thí sinh theo từng câu hỏi. Ví dụ: 1 A, 2 C,...
4. **toeic_question_and_answer.py** là code đọc file bộ đề thi và xuất ra file **toeic_exam1_question_and_answer.xlsx**.
5. toeic_exam1_question_and_answer.xlsx là file excel chứa bộ câu hỏi và câu trả lời đã được lọc từ bộ đề.


