# Đọc file excel TOEIC_Exam1_Done.xlsx.
# Liệt kê các phần của đề, số câu hỏi mỗi phần.
# Trình bày lại dưới dạng: No (Câu hỏi thứ) --> Question (Nội dung câu hỏi) --> A | B | C | D (Các tùy chọn trả lời)

import pandas as pd
import numpy as np

# 1. Liệt kê tất cả các sheets của file.
# 2. Tạo một vòng lặp để đọc từng sheet.
# 3. Đối với sheet1 và sheet2, các tùy chọn trả lời là các cột feedback_A|B|C|D.
# 4. Đối với các sheet còn lại (sheet 3-7), các tùy chọn trả lời là các cột A|B|C|D.
# Các sheet này có bộ câu hỏi được đánh số theo cột No.
# Các hàng audio không được đánh số thứ tự thì dùng điều kiện để loại bỏ.
# 5. Sau mỗi vòng lặp thì nối các DataFrame lại với nhau và xuất ra một file Excel tổng hợp.


# Tạo Object là file excel có tên 'TOEIC_Exam1_Done.xlsx'.
# xls.sheet_names trả về danh sách các sheet của file.
xls = pd.ExcelFile('TOEIC_Exam1_Done.xlsx')             
print("Đề thi gồm có: " + str(len(xls.sheet_names)) + " phần, gồm: ")       
print("- " + ", ".join(xls.sheet_names))    

num_questions = 0               # Biến đếm tổng số câu hỏi của bộ đề.
df_total = pd.DataFrame()       # Tạo một DataFrame rỗng để nối các DataFrame tương ứng với mỗi sheet được đọc.


# Đọc từng sheet. Tạo 1 DataFrame từ các sheet này, gọi là df.
# Đối với sheet1 và sheet2, các tùy chọn trả lời là các cột feedback A, B, C, D.
# - Tạo một DataFrame gồm cột Question và các cột Feedback từ df.
# - Đổi tên các cột Feedback này thành A, B, C, D để đồng nhất với các sheet sau.
# - Sau đó nối DataFrame đã chỉnh sửa này với DataFrame tổng (df_total). 
# Đối với sheet3-7, các tùy chọn trả lời là các cột A, B, C, D.
# - Các dòng câu hỏi là những dòng được đánh số thứ tự trong cột No.
# - Dòng nào không được đánh số thì loại ra bằng điều kiện df[df.No>=0].
# - Tương tự tạo 1 DataFrame gồm các cột Question, A, B, C và D từ df[df.No>=0].
# - Nối các DataFrame này với DataFrame tổng (df_total)

for sheet_name in xls.sheet_names:
    df = pd.read_excel('TOEIC_Exam1_Done.xlsx', sheet_name=sheet_name)

    if sheet_name in ['Part1', 'Part2']:       
        num_questions += len(df)
        print("  + " + sheet_name + " gồm có " + str(df.shape[0]) + " câu hỏi.")
        df1 = df[['Question', 'Feedback_A', 'Feedback_B', 'Feedback_C', 'Feedback_D']]
        df1_rename = df1.rename(columns={"Feedback_A": "A", "Feedback_B": "B", "Feedback_C": "C", "Feedback_D": "D"})
        df_total = df_total.append(df1_rename, ignore_index=True)
    else:
        df2 = df[df.No >= 0][['Question', 'A', 'B', 'C', 'D']]
        print("  + " + sheet_name + ' gồm có ' + str(len(df2)) + ' câu hỏi.')
        num_questions += len(df2)
        df_total = df_total.append(df2, ignore_index=True)

# Sau khi thực hiện xong vòng lặp thì DataFrame tổng lúc này đã gồm tất cả dữ liệu đã lọc được từ tất cả các sheet.
# Đặt lại số thứ tự từ 1-200 tương ứng với từng câu hỏi.
# Đặt tên cho cột thứ tự là 'No'.
# Xuất DataFrame tổng thành file Excel.
# In ra 5 dòng đầu của file để kiểm thử.
# In ra tổng số câu hỏi.

df_total.index = np.arange(1,len(df_total)+1)
df_total.index.names = ['No']
#df_total.to_excel('toeic_exam1_questions_and_answers.xlsx')
print(df_total.head())
print("Tổng số câu hỏi: " + str(num_questions) + ".")

# OUTPUT:

# Đề thi gồm có: 7 phần, gồm: 
# - Part1, Part2, Part3, Part4, Part5, Part6, Part7
#   + Part1 gồm có 6 câu hỏi.
#   + Part2 gồm có 25 câu hỏi.
#   + Part3 gồm có 39 câu hỏi.
#   + Part4 gồm có 30 câu hỏi.
#   + Part5 gồm có 30 câu hỏi.
#   + Part6 gồm có 16 câu hỏi.
#   + Part7 gồm có 54 câu hỏi.
#                                              Question  ...                                              D
# No                                                     ...
# 1   {"image": "${rootURL}/media/part1/1.png", "aud...  ...                        She's closing a cabinet
# 2   {"image": "${rootURL}/media/part1/2.png", "aud...  ...                     The man's unlocking a gate
# 3   {"image": "${rootURL}/media/part1/3.png", "aud...  ...         The door to a house has been left open
# 4   {"image": "${rootURL}/media/part1/4.png", "aud...  ...  Some diners are seated across from each other
# 5   {"image": "${rootURL}/media/part1/5.png", "aud...  ...    Some shoes are being lined up under a bench

# [5 rows x 5 columns]
# Tổng số câu hỏi: 200.



# Reference:
# 1. Cấu trúc đề thi Toeic 2 kỹ năng: https://tienganhmoingay.com/thong-tin-toeic/cau-truc-de-thi-toeic/
# 2. DataFrame rename column: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.rename.html
# 3. Nối 2 DataFrame: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.append.html
# 4. Đặt tên cho thứ tự câu hỏi: https://stackoverflow.com/questions/19851005/rename-pandas-dataframe-index
# 5. https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html
