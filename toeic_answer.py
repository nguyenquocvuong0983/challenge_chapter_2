# Đọc file excel TOEIC_Exam1_Done.xlsx.
# Liệt kê các phần của đề, số câu hỏi mỗi phần.
# Trình bày lại thành file excel có 2 cột: No (Câu hỏi thứ) --> Answer (Đáp án) A/B/C/D
# Các bước thực hiện tương tự như file toeic_question_and_answer.py nhưng chỉ lấy cột Answer là đáp án được chọn của mỗi câu hỏi.


import pandas as pd
import numpy as np

xls = pd.ExcelFile('TOEIC_Exam1_Done.xlsx')
print("Đề thi gồm có: " + str(len(xls.sheet_names)) + " phần, gồm: ")
print("- " + ", ".join(xls.sheet_names))

num_questions = 0
df_total = pd.DataFrame()

for sheet_name in xls.sheet_names:
    df = pd.read_excel('TOEIC_Exam1_Done.xlsx', sheet_name=sheet_name)

    if sheet_name in ['Part1', 'Part2']:       
        num_questions += len(df)
        print("  + " + sheet_name + " gồm có " + str(df.shape[0]) + " câu hỏi.")
        df1 = df[["Answer"]]
        df_total = df_total.append(df1, ignore_index=True)
    else:
        df2 = df[df.No >= 0][["Answer"]]
        print("  + " + sheet_name + ' gồm có ' + str(len(df2)) + ' câu hỏi.')
        num_questions += len(df2)
        df_total = df_total.append(df2, ignore_index=True)

df_total.index = np.arange(1,len(df_total)+1)
df_total.index.names = ['No']
df_total.to_excel('toeic_exam1_answers.xlsx')
print(df_total.head())
print("Tổng số câu hỏi: " + str(num_questions) + ".")

# OUTPUT:
# Đề thi gồm có: 7 phần, gồm: 
# - Part1, Part2, Part3, Part4, Part5, Part6, Part7
#   + Part1 gồm có 6 câu hỏi.
#   + Part2 gồm có 25 câu hỏi.
#   + Part3 gồm có 39 câu hỏi.
#   + Part4 gồm có 30 câu hỏi.
#   + Part5 gồm có 30 câu hỏi.
#   + Part6 gồm có 16 câu hỏi.
#   + Part7 gồm có 54 câu hỏi.

#    Answer
# No
# 1       B
# 2       C
# 3       B
# 4       D
# 5       C
# Tổng số câu hỏi: 200.


# Reference:
# 1. Cấu trúc đề thi Toeic 2 kỹ năng: https://tienganhmoingay.com/thong-tin-toeic/cau-truc-de-thi-toeic/
# 2. DataFrame rename column: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.rename.html
# 3. Nối 2 DataFrame: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.append.html
# 4. Đặt tên cho column index: https://stackoverflow.com/questions/19851005/rename-pandas-dataframe-index
# 5. https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html
